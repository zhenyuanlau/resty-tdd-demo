# Resty TDD Demo

## Env

### Install OpenResty

```bash
# refer https://openresty.org/en/installation.html
brew install openresty/brew/openresty
```

### Install Test::Nginx

```bash
# refer https://openresty.gitbooks.io/programming-openresty/content/testing/test-nginx.html
sudo cpan Test::Nginx
```

## Test

```bash
make test
```

## Start

```bash
make start

curl 'http://127.0.0.1:1984/t'
```

## Stop

```bash
make stop
```
