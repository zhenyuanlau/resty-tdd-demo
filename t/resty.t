use Test::Nginx::Socket 'no_plan';

run_tests();

__DATA__

=== TEST 1: hello, resty

--- config

location = /t {

  content_by_lua_block {
    ngx.say 'hello, resty'
  }

}

--- request
GET /t

--- response_body
hello, resty

=== TEST 2: hello, resty module

--- ONLY

--- http_config

lua_package_path "./lua/?.lua;;";

--- config

location = /t {
  
  content_by_lua_block {
    local resty = require 'resty'
    ngx.say(resty:hello())
  }

}

--- request
GET /t

--- response_body 
hello, resty
