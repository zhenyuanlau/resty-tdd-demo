OPENRESTY_PREFIX := `brew --prefix openresty`

.PHONY: test start stop

start:
	@cp t/servroot/conf/nginx.conf conf/
	@openresty -p `pwd` -c `pwd`/conf/nginx.conf

stop:
	@lsof -ti :1984 | xargs kill -9

test:
	PATH=$(OPENRESTY_PREFIX)/nginx/sbin:$$PATH prove -r t/